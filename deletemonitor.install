<?php
/**
 * @file
 * Install, update, and uninstall functions for the deletemonitor module.
 */

/**
 * Implementation of hook_schema()
 */
function deletemonitor_schema() {
  $schema['deletemonitor'] = array(
    'description' => 'Stores the data about Views that have been deleted.',
    'fields' => array(
      'dmid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The auto incremented delete monitor id of the current record.',
      ),
      'uid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The user ID of the user who submitted the form.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'description' => 'The name of the view that has been deleted.',
      ),
      'description' => array(
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'normal',
        'description' => 'The description of the view.',
      ),
      'time' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The timestamp of when the view was deleted',
      ),
      'type' => array(
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'description' => 'Type being deleted, e.g. View, Content type, ImageCache',
      ),
    ),
    'indexes' => array(
      'dmid' => array('dmid'),
      'uid' => array('uid'),
    ),
    'primary key' => array('dmid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall()
 */
function deletemonitor_uninstall() {
  variable_del('deletemonitor_types');
}
